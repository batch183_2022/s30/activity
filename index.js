// Include express module
const express = require("express");

// Mongoose is a package that allows creation of schemas to model our data structure.
//Also has access to a number of methods for manipulating our database.
const mongoose = require("mongoose");

// Setup the express server
const app = express();

// List the post where the server will listen
const port = 3001;

// MongoDB Connection
// Connect to the database by passing in your connection string.
/*
	Syntax:
		mongoose.connect("<MongoDB Atlas Connection String>", {userNewUrlParser: true, useUnifiedTopology: true});
*/

mongoose.connect("mongodb+srv://admin:admin@cluster0.scuimpl.mongodb.net/b183_to-do?retryWrites=true&w=majority",{useNewUrlParser: true, useUnifiedTopology: true});

// Set a notification for connection success or error.
let db = mongoose.connection;

// If a connection error occured, it will be output in the console.
// Console.error.bind(console) allows us to print the error in the browser consoles and in the terminal.
db.on("error", console.error.bind(console, "connection error"))

db.once("open", ()=>console.log("We're connected to the cloud database."))


// Mongoose Schema
// Determine the structure of the document to be written in the database.
// Schemas act as blueprint to our data.
const taskSchema = new mongoose.Schema({
	// name: {
	// 	type: String   // for long method
	// }

	//Name of the task
	name: String,  // String is a shorthand for name:{type:string}

	//Status task (Complete, Pending, Incomplete)
	Status:{
		type: String,

		//Default value are the predefined values for a field if we don't put any value.
		default: "Pending"
	}
})

// Mongoose Model
// Model uses Schemas and they act as the middleman from the server (JS code) to our database.
/*
	Syntax:
		const modelName = mongoose.model("colletionName", mongooseSchema);
*/
// Model must be in singular form and capitalize the first letter.
// Using Mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form when creating a collection.
const Task = mongoose.model("Task", taskSchema);

//Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Route for creating a task
// Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return an error        // To avoid duplication
		- If the task doesn't exist in the database, we add it in the database  // To avoid duplication
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object

	Task is duplicated if:
	- result from the query not equal to null. 
	- req.body.name is equal to result.name.
*/
app.post("/tasks", (req, res)=>{
	// Call back functions in mongoose methods are programmed this way: 
	 		// First parameter store the error.
	 		// Second parameter return the result.
	 		// req.body.name = eat
	 		// Task.findOne({name:eat})
	Task.findOne({name:req.body.name}, (err, result)=>{
		console.log(result);

		// If a documents was found and the documents matches the information sent via client / postman
		if(result != null && req.body.name == result.name){
			// Return a message to the client / postman
			return res.send("Duplicate task found!");
		}
		// If no document was found or no duplicate
		else{
			// Create a new task object and save to database
			let newTask = new Task({
				name: req.body.name
			});

			// The ".save" method will store the information to the database
			// Since the "newTask" was created / instantiated from the Task model that contains the Mongoose Schema, so it will gain access to the save method.

						//error handler // save task
			newTask.save((saveErr, saveTask)=>{
				// If there are errors in saving it will be displayed in the console.
				if(saveErr){
					return console.error(saveErr);
				}
				// If no error found while creating the document it will be save in the database.
				else{
					return res.status(201).send("New task created.");
				}
			})
		}
	})
})

// Business Logic
		/*
		1. Retrieve all the documents
		2. If an error is encountered, print the error
		3. If no errors are found, send a success status back to the client/Postman and return an array of documents
		*/
app.get("/tasks", (req, res)=>{
	Task.find({}, (err, result)=>{
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).send(result);
		}
	})
})





//////////////////////////////////////////////////////////////////////////////////////////

//Activity
/*

	Instructions s30 Activity:
	1. Create a User schema.
	2. Create a User model.
	3. Create a POST route that will access the "/signup" route that will create a user.
	4. Process a POST request at the "/signup" route using postman to register a user.
	5. Create a git repository named S30.
	6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	7. Add the link in Boodle.
*/
//Register a user

/*
	Business Logic:

	1. Add a functionality to check if there are duplicate tasks
		- If the user already exists in the database, we return an error
		- If the user doesn't exist in the database, we add it in the database
	2. The user data will be coming from the request's body
		- Note: Make sure that the username and password from the request is not empty.
	3. Create a new User object with a "username" and "password" fields/properties
*/


const userSchema = new mongoose.Schema({
	userName: String,
	password: String
})

const User = mongoose.model("User", userSchema);

//Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.post("/signup", (req, res)=>{
	
	User.findOne({username:req.body.username}, (err, result) =>{
		console.log(result);

	if(result != null && req.body.username == result.username){
			return res.send("Duplicate user found!");
		}
		
		else{
			
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, saveUser) =>{
				
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New user created.");

				
				}
			})
		}
	})

})

app.get("/signup", (req,res)=>{
	User.find({}, (err, result) =>{
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).send(result); 
		}
	})
})


// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));